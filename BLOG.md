
# 0 &mdash; Architecture

```plantuml
[Telegram Bot] -> [Tex Parser] 
[Tex Parser] -> [Layout Engine]
[Layout Engine] --> (Unicode) 
(Unicode) -> [Telegram Bot]
```

# 1 &mdash; The Telegram message parser

For the parser I used a stack based approach, there is a class that holds the source of the text and a cursor to remember the current position.

The class [`TexParser`](/parser.py#L57) handles all the stack manipulation and caret movement. In the `parse` function the stack and the current token is initialized as following

```python
self.source = source
self.i = 0
self.stack = [[]]
self.current_token = ''
```

there are many helper fuctions in this class, some manage the current token

```python
def has_next(self):
    return self.i < len(self.source)

def step(self, amount=1):
    self.i += amount
    return self.source[self.i - amount:self.i]

def peek(self, amount=1):
    return self.source[self.i:self.i + amount]

def end_token(self):
    if len(self.current_token) > 0:
        self.stack[-1] += [self.current_token]
        self.current_token = '' 
```

and these other ones the stack

```python
def push_stack(self):
    self.end_token()
    self.stack += [[]]

def pop_stack(self, wrapper = lambda x: x):
    self.end_token()
    self.stack[-2] += [wrapper(self.stack[-1])]
    del self.stack[-1]

def pop_token(self):
    return self.stack[-1].pop()

def push_token(self, token):
    self.stack[-1].append(token)
```

and there is a composite function to accumulate all characters while a predicate holds.

```python
def accumulate_while(self, predicate):
    self.end_token()
    while predicate(self.source, self.i):
        self.current_token += self.step()
    self.end_token()
```

The grammar first parses for normal markdown text by the `parse_text` function

```python
def parse_text(self, end='}'):
    while self.has_next() and self.peek() != end:
        s = self.source
        i = self.i

        # Display and inline scopes
        if s[i:i+2] == '$$':
            self.step(2) # starting '$$'
            self.push_stack()
            self.parse_math(end='$$')
            self.pop_stack(DisplayMath)
            self.step(2) # ending   '$$'
        elif s[i:i+2] == '\[':
            # ...
        elif s[i:i+2] == '\(':
            # ...
        elif s[i] == '$':
            self.step() # starting '$'
            self.push_stack()
            self.parse_math(end='$')
            self.pop_stack(InlineMath)
            self.step() # ending   '$'
        
        # Custom Syntax for bold and italic text
        elif s[i] == '*':
            self.step() # starting '*'
            self.push_stack()
            self.accumulate_while(lambda s, i: s[i] != '*')
            self.pop_stack(Bold)
            self.step() # ending   '*'
        elif s[i] == '_':
            self.step() # starting '_'
            self.push_stack()
            self.accumulate_while(lambda s, i: s[i] != '_')
            self.pop_stack(Italic)
            self.step() # ending   '_'

        else:
            self.current_token += self.step()
```

And then there is the remaing of the code...

```python

WHITESPACE = re.compile(r"\s")
PATTERN_COMMAND = re.compile(r"[a-zA-Z@]")
# TEX_MERGE_2 = [r"\ ", r"\,", r"\;", r"\&"]

@dataclass
class DisplayMath:
    children: List

@dataclass
class InlineMath:
    children: List

@dataclass
class Bold:
    children: List

@dataclass
class Italic:
    children: List

@dataclass
class TexOptionalArgument:
    children: List

@dataclass
class TexRequiredArgument:
    children: List

@dataclass
class TexCommand:
    name: str
    arguments: List

@dataclass
class TexScope:
    children: List

@dataclass
class TexSupscript:
    children: List

@dataclass
class TexSubscript:
    children: List

    def parse_command(self):
        self.step() # '\'
        self.accumulate_while(lambda s, i: PATTERN_COMMAND.match(s[i]))
        command_name = self.pop_token()
        
        self.push_stack()
        while self.peek() == '{' or self.peek() == '[':
            while self.peek() == '[':
                self.step()
                self.push_stack()
                self.parse_math(end=']')
                self.pop_stack(TexOptionalArgument)
                self.step()

            while self.peek() == '{':
                self.step()
                self.push_stack()
                self.parse_math(end='}')
                self.pop_stack(TexRequiredArgument)
                self.step()
        self.pop_stack()
        
        arguments = self.pop_token()
        
        self.push_token(TexCommand(command_name, arguments))

    def parse_single(self):
        
        if self.peek() == '\\':
            self.parse_command()

        elif self.peek(1) == '{':
            self.step()
            self.push_stack()
            self.parse_math(end='}')
            self.pop_stack(TexScope)
            self.step()
        
        else:
            self.current_token += self.step()
            self.end_token()

    def parse_math(self, end='}'):
        while self.has_next() and self.peek(len(end)) != end:
            s = self.source
            i = self.i

            if self.peek(1) == '\\' and not PATTERN_COMMAND.match(self.peek(2)[1]):
                self.end_token()
                self.current_token += self.step(2)
                self.end_token()

            elif self.peek(1) == '\\':
                self.parse_command()
            
            elif self.peek(1) == '{':
                self.step()
                self.push_stack()
                self.parse_math(end='}')
                self.pop_stack(TexScope)
                self.step()

            elif self.peek() == '^':
                self.step()
                self.push_stack()
                self.parse_single()
                self.pop_stack(TexSupscript)

            elif self.peek() == '_':
                self.step()
                self.push_stack()
                self.parse_single()
                self.pop_stack(TexSubscript)

            else:
                char = self.step()
                if not WHITESPACE.match(char):
                    self.current_token += char
                self.end_token()

```

---

