import pathlib
import logging

import dataclasses
import json

from uuid import uuid4

from telegram import InlineQueryResultArticle, ParseMode, InputTextMessageContent
from telegram.ext import Updater, InlineQueryHandler, CommandHandler
from telegram.utils.helpers import escape_markdown

from tex2unicode import source2unicode

HELP_TEXT = """
For short messages (up to 256 characters) you can use @uctexbot inline within any chat, otherwise add me to your group and use /convert

*Supported:* Complete list of all commands coming soon... For now only inline mode is supported.

*Here are some examples:*

‣ @uctexbot The equation $e^x = x$ has no explicit form 
The equation `eˣ = x`

‣ @uctexbot The integral $$\int e^{-x^2} dx$$ has no explicit form
The integral 

`...
...
...`

has no explicit form

‣ @uctexbot The equation $e^x = x$ has no explicit form
The equation `eˣ = x`

`...`
"""

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)

def start(update, context):
    update.message.reply_text('Hi, you can call me everywhere by using @uctexbot !')


def help(update, context):
    update.message.reply_text(HELP_TEXT, parse_mode=ParseMode.MARKDOWN)


def transform_text(text):
    print("Got:", text)
    result = source2unicode({ 'size': 'normal' }, text)
    print(result)
    print("Ret:", result)
    return result

    # class EnhancedJSONEncoder(json.JSONEncoder):
    #     def default(self, o):
    #         if dataclasses.is_dataclass(o):
    #             return dataclasses.asdict(o)
    #         return super().default(o)
    
    # r = json.dumps(TexParser().parse(text), cls=EnhancedJSONEncoder)


    # return r


def inlinequery(update, context):
    """Handle the inline query."""
    query = update.inline_query.query

    message = query if len(query) < 256 else "Sorry too long, add Bot to group"

    results = [
        InlineQueryResultArticle(
            id=uuid4(),
            title=message,
            input_message_content=InputTextMessageContent(
                transform_text(query), parse_mode=ParseMode.MARKDOWN_V2
            )
        )
    ]

    update.inline_query.answer(results)


def convert(update, context):
    update.message.reply_text(
        transform_text(update.message.text[update.message.text.index(' ') + 1:]),
        parse_mode=ParseMode.MARKDOWN_V2
    )

def error(update, context):
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():

    TOKEN = pathlib.Path("./TOKEN").read_text().strip()

    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("convert", convert))
    dp.add_handler(CommandHandler("latex", convert))
    dp.add_handler(CommandHandler("tex", convert))

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))
    dp.add_handler(InlineQueryHandler(inlinequery))

    dp.add_error_handler(error)

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()

