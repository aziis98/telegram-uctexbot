

class BaseTranslator:

    def __init__(self, translator):
        self.translator = translator
        
    def translate(self, context, node):
        return self.translator(context, node)

    def test(self, context):
        return True


class SizeTranslator(BaseTranslator):
    def __init__(self, sizes_sepecification):
        BaseTranslator.__init__(self)
        self.sizes = sizes_sepecification

    def translate(self, context, node):
        return self.sizes[context.size].translate(context, node)

    def test(self, context):
        return context.size in self.sizes.keys() 

def unicode(char):
    return BaseTranslator(lambda context, node: char)

COMMANDS_0 = {

    # Escapes 
    '\\\\':           unicode('\n'),
    '\{':            unicode('{'),
    '\}':            unicode('}'),

    'mid':          unicode(' | '),

    # Mathematical Symbols
    'le':           unicode('≤'),
    'leq':          unicode('≤'),
    'ge':           unicode('≥'),
    'geq':          unicode('≥'),
    'Lt':           unicode('≪'),
    'Gt':           unicode('≫'),
    'ne':           unicode('≠'),
    'equiv':        unicode('≡'),
    'nequiv':       unicode('≢'),
    'neq':          unicode('≠'),
    'infty':        unicode('∞'),
    'empty':        unicode('∅'),
    'nothing':      unicode('∅'),
    'varnothing':   unicode('∅'),
    'forall':       unicode('∀'),
    'exists':       unicode('∃'),
    'nexists':      unicode('∄'),
    'comp':         unicode('∁'),
    'nabla':        unicode('∇'),
    'setminus':     unicode('∖'),
    'in':           unicode('∊'),
    'ni':           unicode('∍'),
    'notin':        unicode('∉'),
    'notni':        unicode('∌'),
    'radic':        unicode('√'),
    'vert':         unicode('∣'),
    'Vert':         unicode('∥'),
    'land':         unicode('∧'),
    'lor':          unicode('∨'),
    'cap':          unicode('∩'),
    'cup':          unicode('∪'),
    'subset':       unicode('⊂'),
    'nsubset':      unicode('⊄'),
    'subseteq':     unicode('⊆'),
    'nsubseteq':    unicode('⊈'),
    'supset':       unicode('⊃'),
    'supset':       unicode('⊅'),
    'supseteq':     unicode('⊇'),
    'nsupseteq':    unicode('⊉'),
    'sim':          unicode('∼'),
    'simeq':        unicode('≃'),
    'simeqq':       unicode('≅'),
    'simneqq':      unicode('≆'),
    'nsimeqq':      unicode('≇'),
    'approx':       unicode('≈'),
    'perp':         unicode('⊥'),
    'models':       unicode('⊨'),
    'lhd':          unicode('⊲'),
    'rhd':          unicode('⊳'),
    'dots':         unicode('…'),
    'cdots':        unicode('⋯'),
    'vdots':        unicode('⋮'),
    'wp':           unicode('℘'),
    'qed':          unicode('∎'),
    'Re':           unicode('ℜ'),
    'Im':           unicode('ℑ'),
    
    # Ebraic
    'aleph':        unicode('ℵ'),
    'beth':         unicode('ℶ'),
    'gimel':        unicode('ℷ'),
    'dalet':        unicode('ℸ'),

    'oplus':        unicode('⊕'),
    'odot':         unicode('⊙'),
    'otimes':       unicode('⊗'),
    'otimes':       unicode('⊛'),
    'cdot':         unicode('⋅'),
    'star':         unicode('⋆'),
    'ast':          unicode('∗'),
    'circ':         unicode('∘'),
    'compose':      unicode('∘'),
    'cdot':         unicode('∙'),
    'pm':           unicode('±'),

    # Pointing
    'leftarrow':   unicode('←'),
    'rightarrow':   unicode('→'),
    'to':           unicode('→'),
    'mapsto':       unicode('↦'),
    'implies':      unicode('⇒'),
    
    # Lower Greek Letters
    'alpha':        unicode('𝛼'),
    'beta':         unicode('𝛽'),
    'gamma':        unicode('𝛾'),
    'delta':        unicode('𝛿'),
    'varepsilon':   unicode('𝜀'),
    'zeta':         unicode('𝜁'),
    'eta':          unicode('𝜂'),
    'theta':        unicode('𝜃'),
    'iota':         unicode('𝜄'),
    'kappa':        unicode('𝜅'),
    'lambda':       unicode('𝜆'),
    'mu':           unicode('𝜇'),
    'nu':           unicode('𝜈'),
    'xi':           unicode('𝜉'),
    'omicron':      unicode('𝜊'),
    'pi':           unicode('𝜋'),
    'rho':          unicode('𝜌'),
    'varsigma':     unicode('𝜍'),
    'sigma':        unicode('𝜎'),
    'tau':          unicode('𝜏'),
    'upsilon':      unicode('𝜐'),
    'varphi':       unicode('𝜑'),
    'chi':          unicode('𝜒'),
    'psi':          unicode('𝜓'),
    'partial':      unicode('𝜕'),
    'epsilon':      unicode('𝜖'),
    'theta':        unicode('𝜗'),
    'varkappa':     unicode('𝜘'),
    'phi':          unicode('𝜙'),
    'varrho':       unicode('𝜚'),
    'varpi':        unicode('𝜛'),
    
    # Upper Greek Letters
    'Alpha':        unicode('𝛢'),
    'Beta':         unicode('𝛣'),
    'Gamma':        unicode('𝛤'),
    'Delta':        unicode('𝛥'),
    'Epsilon':      unicode('𝛦'),
    'Zeta':         unicode('𝛧'),
    'Eta':          unicode('𝛨'),
    'Theta':        unicode('𝛩'),
    'Iota':         unicode('𝛪'),
    'Kappa':        unicode('𝛫'),
    'Lambda':       unicode('𝛬'),
    'Mu':           unicode('𝛭'),
    'Nu':           unicode('𝛮'),
    'Xi':           unicode('𝛯'),
    'Omicron':      unicode('𝛰'),
    'Pi':           unicode('𝛱'),
    'Rho':          unicode('𝛲'),
    'Sigma':        unicode('𝛴'),
    'Tau':          unicode('𝛵'),
    'Upsilon':      unicode('𝛶'),
    'Phi':          unicode('𝛷'),
    'Chi':          unicode('𝛸'),
    'Psi':          unicode('𝛹'),
    'Omega':        unicode('𝛺'),
}

def handle_command_mathbb(command):
    return unicode_toalphabet(''.join(command.arguments[0].children), ALPHABETS['BOLD_CHALK'])

def handle_command_mathcal(command):
    return unicode_toalphabet(''.join(command.arguments[0].children), ALPHABETS['SCRIPT'])

def handle_command_mathfrak(command):
    return unicode_toalphabet(''.join(command.arguments[0].children), ALPHABETS['FRAKTUR'])

COMMANDS_1 = {
    'mathbb': handle_command_mathbb,
    'mathcal': handle_command_mathcal,
    'mathscr': handle_command_mathcal,
    'mathfrak': handle_command_mathfrak
}

ALPHABETS = {
    'ASCII': 'abcdefghijklmnopqrtsuvwxyzABCDEFGHIJKLMNOPQRTSUVWXYZ',
    'ITALIC': '𝑎𝑏𝑐𝑑𝑒𝑓𝑔ℎ𝑖𝑗𝑘𝑙𝑚𝑛𝑜𝑝𝑞𝑟𝑠𝑡𝑢𝑣𝑤𝑥𝑦𝑧𝐴𝐵𝐶𝐷𝐸𝐹𝐺𝐻𝐼𝐽𝐾𝐿𝑀𝑁𝑂𝑃𝑄𝑅𝑆𝑇𝑈𝑉𝑊𝑋𝑌𝑍',
    'BOLD_CHALK': '𝕒𝕓𝕔𝕕𝕖𝕗𝕘𝕙𝕚𝕛𝕜𝕝𝕞𝕟𝕠𝕡𝕢𝕣𝕤𝕥𝕦𝕧𝕨𝕩𝕪𝕫𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ',
    'SCRIPT': '𝒶𝒷𝒸𝒹ℯ𝒻ℊ𝒽𝒾𝒿𝓀ℓ𝓂𝓃ℴ𝓅𝓆𝓇𝓈𝓉𝓊𝓋𝓌𝓍𝓎𝓏𝒜ℬ𝒞𝒟ℰℱ𝒢ℋℐ𝒥𝒦ℒℳ𝒩𝒪𝒫𝒬ℛ𝒮𝒯𝒰𝒱𝒲𝒳𝒴𝒵',
    'FRAKTUR': '𝖆𝖇𝖈𝖉𝖊𝖋𝖌𝖍𝖎𝖏𝖐𝖑𝖒𝖓𝖔𝖕𝖖𝖗𝖘𝖙𝖚𝖛𝖜𝖝𝖞𝖟𝕬𝕭𝕮𝕯𝕰𝕱𝕲𝕳𝕴𝕵𝕶𝕷𝕸𝕹𝕺𝕻𝕼𝕽𝕾𝕿𝖀𝖁𝖂𝖃𝖄𝖅'
}

import re

TELEGRAM_ESCAPES = re.compile(r"""([_*[\]()~`>#+-=|{}\.!])""")

def telegram_escape(s):
    return TELEGRAM_ESCAPES.sub(r"\\\1", s)

def unicode_toalphabet(string, alphabet):
    def swap_alphabet(char):
        return alphabet[ALPHABETS['ASCII'].index(char)] if char in ALPHABETS['ASCII'] else char

    return ''.join(map(swap_alphabet, string))

def unicode_underline(string):
    UNDERLINE = '\u0332'
    return UNDERLINE.join(string)

from parser import *
from pprint import pprint

def source2unicode(context, source):
    
    def text2markdown(node):
        return {
            
            Bold: lambda: '*' + ''.join(node.children) + '*',
            Italic: lambda: '_' + ''.join(node.children) + '_',
            InlineMath: lambda: '`' + tex2unicode(node) + '`',

            str: lambda: telegram_escape(node),

        }[node.__class__]()


    def tex2unicode(node):

        def handle_command(command):
            
            # print(command)

            if node.name in COMMANDS_0:
                return COMMANDS_0[node.name].translate(context, node)
            elif node.name in COMMANDS_1:
                return COMMANDS_1[node.name](command)
            else:
                return '`*\\\\' + node.name + '*`'

        return {
            
            InlineMath: lambda: ''.join(map(tex2unicode, node.children)),

            TexCommand: lambda: handle_command(node),

            # TexSupscript: lambda: '^(' + ''.join(node.children) + ')' if len(node.children) > 1 else '^' + ''.join(node.children), 
            # TexSubscript: lambda: '_(' + ''.join(node.children) + ')' if len(node.children) > 1 else '_' + ''.join(node.children), 

            list: lambda: ''.join(map(tex2unicode, node)),
            str: lambda: unicode_toalphabet(node, ALPHABETS['ITALIC'])

        }.get(node.__class__, lambda: ''.join(node.children))()


    ast = TexParser().parse(source)

    # pprint(ast)

    return ''.join(map(text2markdown, ast))


def main():
    print(source2unicode({ 'size': 'normal' }, """Questo è un _messaggio_ di $\{ x \in X | \\varphi(x) \}$ prova"""))

if __name__ == "__main__":
    main()