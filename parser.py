
from typing import List
from dataclasses import dataclass

import dataclasses
import json
import re


WHITESPACE = re.compile(r"\s")
PATTERN_COMMAND = re.compile(r"[a-zA-Z@]")
# TEX_MERGE_2 = [r"\ ", r"\,", r"\;", r"\&"]


@dataclass
class DisplayMath:
    children: List

@dataclass
class InlineMath:
    children: List

@dataclass
class Bold:
    children: List

@dataclass
class Italic:
    children: List

@dataclass
class TexOptionalArgument:
    children: List

@dataclass
class TexRequiredArgument:
    children: List

@dataclass
class TexCommand:
    name: str
    arguments: List

@dataclass
class TexScope:
    children: List

@dataclass
class TexSupscript:
    children: List

@dataclass
class TexSubscript:
    children: List


class TexParser():
    """Parses TeX source code using the `parse` function"""

    def accumulate_while(self, predicate):
        """Accumulates chars until the predicate becomes false"""
        self.end_token()
        while predicate(self.source, self.i):
            self.current_token += self.step()
        self.end_token()


    def has_next(self):
        return self.i < len(self.source)

    def step(self, amount=1):
        self.i += amount
        return self.source[self.i - amount:self.i]

    def peek(self, amount=1):
        return self.source[self.i:self.i + amount]


    def push_stack(self):
        self.end_token()
        self.stack += [[]]

    def pop_stack(self, wrapper = lambda x: x):
        self.end_token()
        self.stack[-2] += [wrapper(self.stack[-1])]
        del self.stack[-1]

    def end_token(self):
        if len(self.current_token) > 0:
            # pprint.pprint(self.stack)
            self.stack[-1] += [self.current_token]
            self.current_token = '' 

    def pop_token(self):
        return self.stack[-1].pop()

    def push_token(self, token):
        self.stack[-1].append(token)


    def parse_command(self):
        self.step() # '\'
        self.accumulate_while(lambda s, i: PATTERN_COMMAND.match(s[i]))
        command_name = self.pop_token()
        
        self.push_stack()
        while self.peek() == '{' or self.peek() == '[':
            while self.peek() == '[':
                self.step()
                self.push_stack()
                self.parse_math(end=']')
                self.pop_stack(TexOptionalArgument)
                self.step()

            while self.peek() == '{':
                self.step()
                self.push_stack()
                self.parse_math(end='}')
                self.pop_stack(TexRequiredArgument)
                self.step()
        self.pop_stack()
        
        arguments = self.pop_token()
        
        self.push_token(TexCommand(command_name, arguments))

    def parse_single(self):
        
        if self.peek() == '\\':
            self.parse_command()

        elif self.peek(1) == '{':
            self.step()
            self.push_stack()
            self.parse_math(end='}')
            self.pop_stack(TexScope)
            self.step()
        
        else:
            self.current_token += self.step()
            self.end_token()

    def parse_math(self, end='}'):
        while self.has_next() and self.peek(len(end)) != end:
            s = self.source
            i = self.i

            # Escape codes
            if self.peek(1) == '\\' and not PATTERN_COMMAND.match(self.peek(2)[1]):
                self.end_token()
                self.current_token += self.step(2)
                self.end_token()

                escape_code = self.pop_token()
                self.push_token(TexCommand(escape_code, []))

            # Commands
            elif self.peek(1) == '\\':
                self.parse_command()
            
            # Scoped blocks
            elif self.peek(1) == '{':
                self.step()
                self.push_stack()
                self.parse_math(end='}')
                self.pop_stack(TexScope)
                self.step()

            # Superscripts
            elif self.peek() == '^':
                self.step()
                self.push_stack()
                self.parse_single()
                self.pop_stack(TexSupscript)

            # Subscripts
            elif self.peek() == '_':
                self.step()
                self.push_stack()
                self.parse_single()
                self.pop_stack(TexSubscript)

            # Any remaining non whitespace character
            else:
                char = self.step()
                if not WHITESPACE.match(char):
                    self.current_token += char
                self.end_token()

    def parse_text(self, end='}'):
        
        while self.has_next() and self.peek() != end:
            s = self.source
            i = self.i

            # Display and inline scopes
            if s[i:i+2] == '$$':
                self.step(2) # starting '$$'
                self.push_stack()
                self.parse_math(end='$$')
                self.pop_stack(DisplayMath)
                self.step(2) # ending   '$$'

            elif s[i:i+2] == '\[':
                self.step(2) # starting '\['
                self.push_stack()
                self.parse_math(end='\]')
                self.pop_stack(DisplayMath)
                self.step(2) # ending   '\]' 

            elif s[i:i+2] == '\(':
                self.step(2) # starting '\('
                self.push_stack()
                self.parse_math(end='\)')
                self.pop_stack(InlineMath)
                self.step(2) # ending   '\)'

            elif s[i] == '$':
                self.step() # starting '$'
                self.push_stack()
                self.parse_math(end='$')
                self.pop_stack(InlineMath)
                self.step() # ending   '$'

            # Custom Syntax for bold and italic
            elif s[i] == '*':
                self.step() # starting '*'
                self.push_stack()
                self.accumulate_while(lambda s, i: s[i] != '*')
                self.pop_stack(Bold)
                self.step() # ending   '*'

            elif s[i] == '_':
                self.step() # starting '_'
                self.push_stack()
                self.accumulate_while(lambda s, i: s[i] != '_')
                self.pop_stack(Italic)
                self.step() # ending   '_'

            else:
                self.current_token += self.step()

    def parse(self, source):
        
        self.source = source
        self.i = 0
        self.stack = [[]]
        self.current_token = ''
        
        self.parse_text()
        self.end_token()

        return self.stack[0]


def main():
    r = TexParser().parse(r"""
        \[ \sum_{i=1}^{\infty} \frac{1}{n^s} = \prod_p \frac{1}{1 - p^{-s}} \]
    """)
    
    class EnhancedJSONEncoder(json.JSONEncoder):
        def default(self, o):
            if dataclasses.is_dataclass(o):
                return dataclasses.asdict(o)
            return super().default(o)

    print(json.dumps(r, indent=2, cls=EnhancedJSONEncoder))


if __name__ == '__main__':
    main()

